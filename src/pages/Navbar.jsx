import React from 'react'
import { NavLink } from 'react-router-dom'

function Navbar() {
    return (
        <nav className="navbar navbar-expand-xl navbar-dark bg-primary py-3">
        <a className="navbar-brand" href="#">Approval Management System</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
            <li className="nav-item active">
                <NavLink to = "home" className="nav-link" href="#">Home <span className="sr-only">(current)</span></NavLink>
            </li>
            <li className="nav-item">
                <NavLink to = "notifikasi" className="nav-link" href="#">Notifikasi</NavLink>
            </li>
            <li className="nav-item">
                <NavLink to = "arsip" className="nav-link" href="#">Arsip</NavLink>
            </li>
            <li className="nav-item">
                <NavLink to = "akun" className="nav-link">Akun</NavLink>
            </li>
            </ul>
        </div>
        </nav>
    )
}

export default Navbar