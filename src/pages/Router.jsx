import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import Login from '../pages/auth/Login'
import Register from '../pages/auth/Register'
import Home from './Home'
import Notifikasi from './Notifikasi'
import Arsip from './Arsip'
import Detaildokumen from './Detaildokumen'

function Router(){
    return (
        <BrowserRouter>
           
            <Route exact path="/" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/home" component={Home}/>
            <Route exact path="/notifikasi" component={Notifikasi}/>
            <Route exact path="/arsip" component={Arsip}/>
            <Route exact path="/detail-dokumen" component={Detaildokumen}/>
        </BrowserRouter>
    )
}

export default Router