import React from 'react'
import Navbar from './Navbar';
import { Container, Button, Alert } from 'react-bootstrap'

function Notifikasi() {
    return (
        <>
        <Navbar/>
        <Container className="p-4">
            <h2>Notifikasi</h2>
            <br />
            <Alert variant="primary">
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading"><b>Dokumen 1</b> telah di-approve!</h4>
                    <hr/>
                    <span className="float-right">
                        <small><b>09.00</b></small>
                    </span>
                </div>
                <hr />
                <span>
                    <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary" role="button">Lihat dokumen</a>
                </span>
            </Alert>
            <Alert variant="primary">
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading"><b>Dokumen 2</b> telah di-approve!</h4>
                    <hr/>
                    <span className="float-right">
                        <small><b>Just Now</b></small>
                    </span>
                </div>
                <hr />
                <span>
                    <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary" role="button">Lihat dokumen</a>
                </span>
            </Alert>
            <Alert variant="primary">
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading"><b>Dokumen 3</b> ditolak!</h4>
                    <hr/>
                    <span className="float-right">
                        <small><b>Just Now</b></small>
                    </span>
                </div>
                <hr />
                <span>
                <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary" role="button">Lihat keterangan</a>
                </span>
            </Alert>
        </Container>
        </>
    )
}

export default Notifikasi