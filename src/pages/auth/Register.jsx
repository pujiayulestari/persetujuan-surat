import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel, NavLink, Image } from "react-bootstrap";
import './Auth.css';
import { useHistory } from "react-router-dom";
import logo2 from '../../img/register.png';


function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  function validateForm() {
    return email.length > 0 && password.length > 2;
  }

  function onSubmit(e) {
    e.preventDefault();
    if (email == "admin@gmail.com" && password == "admin") { 
    history.push("/home");
    }
  }

  return (
    <div className="login-container">
      <div className="login-card">
        <div className="container">
          <div className="row ">
            <div className="col-md-6 align-self-center">

              
              <Image src={logo2} width="100%"></Image>
            </div>
            <div className="col-md-6" >
              <div className="Login">
                <h1 className="text-center">Daftar</h1>
                <form onSubmit={onSubmit}>
                <FormGroup controlId="email">
                    <FormControl
                      autoFocus
                      type="email"
                      value={email}
                      onChange={e => setEmail(e.target.value)}
                      placeholder="Email"
                    />
                  </FormGroup>
                  <FormGroup controlId="email">
                    <FormControl
                      autoFocus
                      type="email"
                      value={email}
                      onChange={e => setEmail(e.target.value)}
                      placeholder="Username"
                    />
                  </FormGroup>
                  <FormGroup controlId="password">
                    <FormControl
                      value={password}
                      onChange={e => setPassword(e.target.value)}
                      type="password"
                      placeholder="Password"
                    />
                  </FormGroup>
                  <FormGroup controlId="password">
                    <FormControl
                      value={password}
                      onChange={e => setPassword(e.target.value)}
                      type="password"
                      placeholder="Re-enter password"
                    />
                  </FormGroup>
                  <div class="text-center">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Daftar</button>
                  </div>
                  <hr></hr>
                  <div class="text-center">
                    <p className="text-center">Sudah memiliki akun? <a href="/"> Login </a></p>
                  </div>
                </form>
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register