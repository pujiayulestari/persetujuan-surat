import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel, NavLink, Image } from "react-bootstrap";
import './Auth.css';
import { useHistory } from "react-router-dom";
import logo1 from '../../img/login.png';



function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function onSubmit(e) {
    e.preventDefault();
    if (email == "admin@gmail.com" && password == "admin") { 
    history.push("/home");
    }
  }

  return (
    <div className="login-container">
      <div className="login-card">
        <div className="container">
          <div className="row ">
            <div className="col-md-6 align-self-center">

              
              <Image src={logo1} width="100%"></Image>
            </div>
            <div className="col-md-6" >
              <div className="Login">
                <h1 className="text-center">Login</h1>
                <form onSubmit={onSubmit}>
                  <FormGroup controlId="email">
                    <FormControl
                      autoFocus
                      type="email"
                      value={email}
                      onChange={e => setEmail(e.target.value)}
                      placeholder="Username"
                    />
                  </FormGroup>
                  <FormGroup controlId="password">
                    <FormControl
                      value={password}
                      onChange={e => setPassword(e.target.value)}
                      type="password"
                      placeholder="Password"
                    />
                  </FormGroup>
                  <div class="text-center">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                  </div>
                  <hr></hr>
                  <div class="text-center">
                    <p className="text-center">Belum memiliki akun? <a href="/register"> Daftar </a></p>
                  </div>
                </form>
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login