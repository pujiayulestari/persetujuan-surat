import React from 'react'
import Navbar from './Navbar'
import { Container, Button, Table, Badge } from 'react-bootstrap'


function Arsip() {
    return (
        <>
        <Navbar/>
        
        <Container className="p-4">
            <h2>Arsip</h2>
            <a href="/detail-dokumen" className="btn btn-success float-right" role="button">Unggah dokumen</a>
            <br /><br /><br />
            <Table striped bordered hover width="100%">
                <thead>
                    <tr>
                        <th width="4%">#</th>
                        <th width="17%">Kode dokumen</th>
                        <th>Nama dokumen</th>
                        <th width="19%">Status</th>
                        <th width="17%">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>dok01</td>
                        <td>Dokumen 1</td>
                        <td><Badge variant="success">Approved</Badge></td>
                        <td>
                            <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary mx-2" role="button">Lihat detail</a>
                            <a href="#" className="btn btn-sm btn-outline-danger" role="button">Hapus</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>dok02</td>
                        <td>Dokumen 2</td>
                        <td><Badge variant="warning">Terkirim. Menunggu approval</Badge></td>
                        <td>
                            <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary mx-2" role="button">Lihat detail</a>
                            <a href="#" className="btn btn-sm btn-outline-danger" role="button">Hapus</a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>dok03</td>
                        <td>Dokumen 3</td>
                        <td><Badge variant="info">Draft</Badge></td>
                        <td>
                            <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary mx-2" role="button">Lihat detail</a>
                            <a href="#" className="btn btn-sm btn-outline-danger" role="button">Hapus</a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>dok04</td>
                        <td>Dokumen 4</td>
                        <td><Badge variant="danger">Ditolak</Badge></td>
                        <td>
                            <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary mx-2" role="button">Lihat detail</a>
                            <a href="#" className="btn btn-sm btn-outline-danger" role="button">Hapus</a>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Container>
        </>
    )
}

export default Arsip
