import React from 'react'
import Navbar from './Navbar'
import { Container, Row, Col, Button, Alert, NavLink, Image, Card } from 'react-bootstrap'

function Home() {
    return (
        <>
            <Navbar/>

            <Container className="py-5 ">
                <h2>Selamat pagi, User</h2>
                <br />
                <Alert variant="primary" dismissible>
                    <Alert.Link href="#">Dokumen 2</Alert.Link> telah di-approve!
                </Alert>

                {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}
                <Row className="p-2">
                    <Col xs={6} md={3} className="p-0">
                        <Card className="m-2">
                            <Card.Body>
                                <Card.Title>Surat dikirim</Card.Title>
                                <Card.Subtitle className="mb-2" style={{ fontSize: 28 }}>20</Card.Subtitle>
                                <a href="/arsip" className="btn btn-sm btn-outline-primary float-right" role="button">Selengkapnya</a>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className="p-0">
                        <Card className="m-2">
                            <Card.Body>
                                <Card.Title>Draft</Card.Title>
                                <Card.Subtitle className="mb-2" style={{ fontSize: 28 }}>5</Card.Subtitle>
                                <a href="/arsip" className="btn btn-sm btn-outline-primary float-right" role="button">Selengkapnya</a>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className="p-0">
                        <Card className="m-2">
                            <Card.Body>
                                <Card.Title>Surat di-approve</Card.Title>
                                <Card.Subtitle className="mb-2" style={{ fontSize: 28 }}>17</Card.Subtitle>
                                <a href="/arsip" className="btn btn-sm btn-outline-primary float-right" role="button">Selengkapnya</a>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className="p-0">
                        <Card className="m-2">
                            <Card.Body>
                                <Card.Title>Surat ditolak</Card.Title>
                                <Card.Subtitle className="mb-2" style={{ fontSize: 28 }}>3</Card.Subtitle>
                                <a href="/arsip" className="btn btn-sm btn-outline-primary float-right" role="button">Selengkapnya</a>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default Home
